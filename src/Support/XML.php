<?php


namespace HttpBase\Support;

use TheNorthMemory\Xml\Transformer;

/**
 * Class XML.
 */
class XML
{
    public static function parse(string $xml): array|null
    {
        return Transformer::toArray($xml);
    }

    public static function build(array $data): string
    {
        return Transformer::toXml($data);
    }
}
