<?php

namespace HttpBase\Exceptions;

/**
 * Class BadRequestException.
 */
class BadRequestException extends Exception
{
}
