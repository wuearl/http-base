<?php

namespace HttpBase\Exceptions;

use Exception as BaseException;

/**
 * Class Exception.
 */
class Exception extends BaseException
{
}
