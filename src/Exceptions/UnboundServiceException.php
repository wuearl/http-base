<?php

namespace HttpBase\Exceptions;

/**
 * Class InvalidConfigException.
 */
class UnboundServiceException extends Exception
{
}
