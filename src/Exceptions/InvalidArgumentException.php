<?php

namespace HttpBase\Exceptions;

/**
 * Class InvalidArgumentException.
 */
class InvalidArgumentException extends Exception
{
}
