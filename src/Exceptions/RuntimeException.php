<?php

namespace HttpBase\Exceptions;

/**
 * Class RuntimeException.
 */
class RuntimeException extends Exception
{
}
