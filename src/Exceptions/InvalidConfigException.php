<?php

namespace HttpBase\Exceptions;

/**
 * Class InvalidConfigException.
 */
class InvalidConfigException extends Exception
{
}
